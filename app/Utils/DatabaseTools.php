<?php

namespace App\Utils;

class DatabaseTools
{
    /**
     * Copy a directory and sub-directories
     * from source $src to destination $dst.
     *
     * Use this not directly, call directoryToStorage()
     * method from DatabaseTools class
     *
     * @param string $src source directory
     * @param string $dst destination directory
     */
    private static function recurseCopy(string $src, string $dst): void
    {
        $dir = opendir($src);
        if (\is_resource($dir)) {
            @mkdir($dst);
            while (false !== ($file = readdir($dir))) {
                if (('.' != $file) && ('..' != $file)) {
                    if (is_dir($src.'/'.$file)) {
                        self::recurseCopy($src.'/'.$file, $dst.'/'.$file);
                    } else {
                        copy($src.'/'.$file, $dst.'/'.$file);
                    }
                }
            }
            closedir($dir);
        }
    }

    /**
     * Helper which use recurseCopy() method from DatabaseTools class.
     *
     * Copy files from database/seeds/storage/ to storage/app/public/
     *
     * @param string $dir         seeds directory to copy to storage
     * @param string $optionToDir specify storage directory, if not set, files will be copy to the root of storage
     */
    public static function directoryToStorage(string $dir, string $optionToDir = null): void
    {
        if (null !== $optionToDir) {
            $optionToDir = "/$optionToDir/";
        }
        $database_files = database_path('seeders/storage/');
        $src = $database_files.$dir;
        if (null !== $optionToDir) {
            $dst = storage_path('app/public/'.$optionToDir);
        } else {
            $dst = storage_path('app/public/'.$dir);
        }
        self::recurseCopy($src, $dst);
    }
}

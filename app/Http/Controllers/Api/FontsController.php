<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Font;

class FontsController extends Controller
{
    public function index()
    {
        $fonts = Font::orderBy('name')->get();

        return $fonts;
    }
}

<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Font extends Model
{
    protected $fillable = [
        'slug',
        'name',
        'from',
        'file_path',
        'import',
        'origin',
    ];

    public $timestamps = false;

    public static function boot()
    {
        static::saving(function (Font $font) {
            if (! empty($font->slug)) {
                return;
            }
            $font->slug = Str::slug($font->name, '-');

            if (! empty($font->import)) {
                return;
            }
            $font->import = 'font-'.Str::slug($font->name, '-');
        });

        // static::deleting(function (Font $font) {
        //     $font->tags()->detach();
        // });

        parent::boot();
    }
}

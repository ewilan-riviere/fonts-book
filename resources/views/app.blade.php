<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- Scripts -->
        {{-- <script src="{{ config('config.url') }}/js/app.js" defer></script> --}}
        <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    @php
        use App\Models\Font;
        $fonts = Font::all();
    @endphp
    @foreach ($fonts as $font)
        <style type="text/css">
            @font-face {
                font-family: {{ $font->name }};
                /* font-style: normal;
                font-weight: normal; */
                src: url("/storage/fonts/{{ $font->file_path }}");
            }
            .font-{{ $font->slug }} {
                font-family: {{ $font->name }};
            }
        </style>
    @endforeach
    <body class="font-sans antialiased">
        @inertia
    </body>
</html>

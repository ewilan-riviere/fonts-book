require('./bootstrap');

import Vue from 'vue';

import { InertiaApp } from '@inertiajs/inertia-vue';
import { InertiaForm } from 'laravel-jetstream';
import PortalVue from 'portal-vue';
import SvgVue from 'svg-vue'

Vue.prototype.$axios = axios

Vue.use(InertiaApp);
Vue.use(InertiaForm);
Vue.use(PortalVue);

import VueClipboard from 'vue-clipboard2'
import VueCollapse from 'vue2-collapse'
// import VueHighlightJS from 'vue-highlightjs'
// import VueMarkdown from 'vue-markdown-v2'
import VueToasted from 'vue-toasted'
// import 'highlight.js/styles/monokai.css'

import VueTailwind from 'vue-tailwind'
import settings from './vue-tailwind/settings'

Vue.use(VueTailwind, settings.settings)

Vue.use(VueClipboard)
Vue.use(VueCollapse)
// Vue.use(VueHighlightJS)
Vue.use(VueToasted, {
  iconPack: 'material',
})
Vue.use(SvgVue)
// Vue.component('vue-markdown', VueMarkdown)
console.log(SvgVue);

const app = document.getElementById('app');

new Vue({
    render: (h) =>
        h(InertiaApp, {
            props: {
                initialPage: JSON.parse(app.dataset.page),
                resolveComponent: (name) => require(`./Pages/${name}`).default,
            },
        }),
}).$mount(app);

const mix = require('laravel-mix');
require('laravel-mix-svg-vue')
require('dotenv')

let url = process.env.APP_URL
url = url.replace(/(^\w+:|^)\/\//, '')
let appUrl = url

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        require('postcss-import'),
        require('tailwindcss'),
    ])
    .svgVue({
      svgPath: 'resources/js/icons',
      extract: false,
      svgoSettings: [{
          removeTitle: true
        },
        {
          removeViewBox: false
        },
        {
          removeDimensions: true
        },
      ],
    })
    .options({
      hmrOptions: {
        host: appUrl, // site's host name
        port: 8080,
      }
    })
    // // fix css files 404 issue
    .webpackConfig({
      // add any webpack dev server config here
      devServer: {
        proxy: {
          host: 'localhost', // host machine ip
          port: 8080,
        },
        watchOptions: {
          aggregateTimeout: 200,
          poll: 5000
        },

      }
    });

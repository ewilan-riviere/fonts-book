<?php

namespace Database\Seeders;

use App\Models\Font;
use Illuminate\Database\Seeder;

class FontSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (array) $fonts = [
            [
                'name'      => 'PT Sans',
                'file_path' => 'pt-sans/PTSans-Regular.ttf',
                'origin'    => 'https://fonts.google.com/specimen/PT+Sans',
            ],
            [
                'name'      => 'Georgia',
                'file_path' => 'georgia/Georgia.ttf',
                'origin'    => 'https://www.downloadfonts.io/georgia-font-family-free',
            ],
            [
                'name'      => 'Lora',
                'file_path' => 'lora/Lora-VariableFont_wght.ttf',
                'origin'    => 'https://fonts.google.com/specimen/Lora',
            ],
            [
                'name'      => 'Helvetica Neue',
                'file_path' => 'helvetica-neue/Helvetica-Neue-Interface-Regular.ttf',
                'origin'    => 'https://www.dafontfree.io/helvetica-neue-font-free',
            ],
            [
                'name'      => 'Fink Heavy',
                'from'      => 'Animal Crossings',
                'file_path' => 'fink-heavy/FinkHeavy.woff',
                'origin'    => 'https://www.fontsmarket.com/font-download/fink-heavy',
            ],
            [
                'name'      => 'Roboto',
                'file_path' => 'roboto/Roboto-Medium.ttf',
                'origin'    => 'https://fonts.google.com/specimen/Roboto',
            ],
            [
                'name'      => 'Quicksand',
                'file_path' => 'quicksand/Quicksand-VariableFont_wght.ttf',
                'origin'    => 'https://fonts.google.com/specimen/Quicksand',
            ],
            [
                'name'      => 'Booter',
                'file_path' => 'booter/booter.ttf',
                'from'      => "L'Autre, P. Bottero",
                'origin'    => 'https://www.dafont.com/booter.font',
            ],
            [
                'name'      => 'Caslon antique',
                'file_path' => 'caslon-antique/caslon-antique.ttf',
                'from'      => 'Ewilan, P. Bottero',
                'origin'    => 'https://www.1001fonts.com/caslon-antique-font.html',
            ],
            [
                'name'      => 'Dancing script',
                'file_path' => 'dancing-script/DancingScript-VariableFont_wght.ttf',
                'origin'    => 'https://fonts.google.com/specimen/Dancing+Script',
            ],
            [
                'name'      => 'Hylia',
                'file_path' => 'hylia/hylia-serif-beta-regular.otf',
                'from'      => 'Zelda',
                'origin'    => 'https://zeldauniverse.net/media/fonts/',
            ],
            [
                'name'      => 'Immortal',
                'file_path' => 'immortal/immortal.ttf',
                'from'      => 'Les Âmes Croisées, P. Bottero',
                'origin'    => 'https://www.dafont.com/immortal.font',
            ],
            [
                'name'      => 'Pelagiad',
                'file_path' => 'pelagiad/pelagiad.ttf',
                'from'      => 'The Elder Scrolls III: Morrowind',
                'origin'    => 'https://pinspiry.com/pelagiad-free-font',
            ],
            [
                'name'      => 'Morpheus',
                'file_path' => 'morpheus/morpheus.woff',
                'from'      => 'His Dark Material',
                'origin'    => 'https://www.dafont.com/fr/morpheus.font',
            ],
            [
                'name'      => 'Permanent Marker',
                'file_path' => 'permanent-marker/permanent-marker-regular.ttf',
                'origin'    => 'https://fonts.google.com/specimen/Permanent+Marker',
            ],
            [
                'name'      => 'Pieces of Eight',
                'file_path' => 'pieces-of-eight/pieces-of-eight.ttf',
                'from'      => 'Pirates of Caribbean',
                'origin'    => 'https://www.dafont.com/fr/pieces-of-eight.font',
            ],
            [
                'name'      => 'Pokémon',
                'file_path' => 'pokemon/pokemon-solid.ttf',
                'from'      => 'Pokémon',
                'origin'    => 'https://www.dafont.com/fr/pokemon.font',
            ],
            [
                'name'      => 'Source Code Pro',
                'file_path' => 'source-code-pro/source-code-pro-regular.ttf',
                'origin'    => 'https://fonts.google.com/specimen/Source+Code+Pro',
            ],
            [
                'name'      => 'Starcraft',
                'file_path' => 'starcraft/starcraft-normal.woff',
                'from'      => 'Starcraft',
                'origin'    => 'https://www.dafont.com/starcraft.font',
            ],
            [
                'name'      => 'Triforce',
                'file_path' => 'triforce/triforce.ttf',
                'from'      => 'Zelda',
                'origin'    => 'https://www.dafont.com/fr/triforce.font',
            ],
            [
                'name'      => 'Big Noodle Titling',
                'file_path' => 'big-noodle-titling/big-noodle-titling.ttf',
                'from'      => 'Overwatch',
                'origin'    => 'https://www.dafont.com/fr/bignoodletitling.font',
            ],
            [
                'name'      => 'Open Sans',
                'file_path' => 'open-sans/OpenSans-Regular.ttf',
                'origin'    => 'https://fonts.google.com/specimen/Open+Sans?selection.family=Open+Sans&sidebar.open=true',
            ],
            [
                'name'      => 'Karla',
                'file_path' => 'karla/Karla-Regular.ttf',
                'origin'    => 'https://fonts.google.com/specimen/Karla',
            ],
            [
                'name'      => 'Ubuntu',
                'file_path' => 'ubuntu/Ubuntu-Regular.ttf',
                'origin'    => 'https://fonts.google.com/specimen/Ubuntu',
            ],
            [
                'name'      => 'Lato',
                'file_path' => 'lato/Lato-Regular.ttf',
                'origin'    => 'https://fonts.google.com/specimen/Lato',
												],
												[
            	"name" => 'Poppins',
													"file_path" => 'poppins/Poppins-Medium.ttf',
													"from" => 'Laravel',
            	"origin" => 'https://fonts.google.com/specimen/Poppins'
												],
												[
            	"name" => 'Raleway',
													"file_path" => 'raleway/Raleway-Bold.ttf',
													"from" => 'Backpack',
            	"origin" => 'https://fonts.google.com/specimen/Raleway'
            ],
            // [
            // 	"name" => '',
            // 	"file_path" => '',
            // 	"origin" => ''
            // ],
        ];

        foreach ($fonts as $key => $font) {
            Font::create($font);
        }
    }
}

<?php

namespace Database\Seeders;

use App\Utils\DatabaseTools;
use Database\Seeders\FontSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DatabaseTools::directoryToStorage('fonts');
        // User::factory(10)->create();
        $this->call(FontSeeder::class);
    }
}
